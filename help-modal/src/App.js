import Button from "react-bootstrap/Button";
import ModalContainer from "./components/ModalContainer";
import { useState } from "react";
import "./App.css";
import SiteNav from "./components/Nav/site/SiteNav";

const styles = {
  btnContainer: {
    display: "flex",
    justifyContent: "center",
  },
  modalBtn: {
    width: "200px",
    margin: "200px auto 0px",
    padding: "20px 40px",
  },
};

function App() {
  // const [navItems] = useState(NavComponents);
  const [navItems] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const [modalShow, setModalShow] = useState(false);

  return (
    <>
      <SiteNav
        navItems={navItems}
        activeIndex={activeIndex}
        setActiveIndex={(navId) => setActiveIndex(navId)}
      />

      <div style={styles.btnContainer}>
        <Button
          variant="primary"
          onClick={() => setModalShow(true)}
          style={styles.modalBtn}
        >
          Launch modal
        </Button>
      </div>

      <ModalContainer show={modalShow} onHide={() => setModalShow(false)} />
    </>
  );
}

export default App;
