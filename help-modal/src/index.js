import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Root from "./routes/root";
import Home from "./routes/Home";
import ActiveTests from "./routes/ActiveTests";
import AddHardware from "./routes/AddHardware";
import CompletedTests from "./routes/CompletedTests";
import NewTest from "./routes/NewTest";
//
import { SectionProvider } from "./context/SectionContext";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    // errorElement: <ErrorPage />,
    children: [
      {
        path: "home",
        element: <Home />,
      },
      {
        path: "hardware/create",
        element: <AddHardware />,
      },
      {
        path: "test/active",
        element: <ActiveTests />,
      },
      {
        path: "test/complete",
        element: <CompletedTests />,
      },
      {
        path: "test/create",
        element: <NewTest />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <SectionProvider>
      <RouterProvider router={router} />
    </SectionProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
