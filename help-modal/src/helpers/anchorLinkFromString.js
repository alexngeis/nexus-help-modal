const anchorLinkFromString = (string) => {
  console.log("anchor input", string);
  // below ignore command is to ensure "\" character is not removed from RegEx
  // prettier-ignore
  const whiteSpaceRegex = /\s/gm

  const spacesToHyphens = string.toLowerCase().replace(whiteSpaceRegex, "-");
  const anchorTagFormat = `#${spacesToHyphens}`;

  return anchorTagFormat;
};

export default anchorLinkFromString;
