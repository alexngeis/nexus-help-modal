// this function takes a React reference object (to an HTML element) and an HTML tag,
//   returns all elements matching the tag from the provided HTML ref element parent
import { allTagsHTML } from "../data/allTagsHTML";

const findElementsByTag = (ref, tag) => {
  // validate against available HTML tags
  if (!allTagsHTML.includes(tag)) return;

  const refElementChildren = [...ref.current.children];
  const filteredElementsByTag = refElementChildren.filter(
    (child) => child.tagName === tag.toUpperCase()
  );

  return filteredElementsByTag;
};

export default findElementsByTag;
