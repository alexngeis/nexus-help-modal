import { createContext, useState } from "react";
import sectionInfoData from "../data/sectionInfoData";

export const SectionContext = createContext();

export const SectionProvider = ({ children }) => {
  const [sectionInfo, setSectionInfo] = useState(sectionInfoData);

  const contextData = {
    sectionInfo,
    setSectionInfo,
  };

  return (
    <SectionContext.Provider value={contextData}>
      {children}
    </SectionContext.Provider>
  );
};
