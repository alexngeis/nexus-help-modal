import React from "react";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import ModalContainer from "../components/Modal/ModalContainer";
import { useOutletContext } from "react-router-dom";

const styles = {
  btnContainer: {
    display: "flex",
    justifyContent: "center",
  },
  modalBtn: {
    width: "200px",
    margin: "200px auto 0px",
    padding: "20px 40px",
  },
};

export default function Home() {
  const [modalShow, setModalShow] = useState(false);

  const { sectionInfo, setSectionInfo, activeSiteNavIndex } =
    useOutletContext();

  const homeSectionInfo = sectionInfo.filter(
    (section) => section.sectionTitle === "Home"
  )[0];

  return (
    <>
      <div style={styles.btnContainer}>
        <Button
          variant="primary"
          onClick={() => setModalShow(true)}
          style={styles.modalBtn}
        >
          Launch {`${homeSectionInfo.sectionTitle} `}modal
        </Button>
      </div>

      <ModalContainer
        show={modalShow}
        onHide={() => setModalShow(false)}
        sectionInfo={sectionInfo}
        setSectionInfo={setSectionInfo}
        activeSiteNavIndex={activeSiteNavIndex}
      />
    </>
  );
}
