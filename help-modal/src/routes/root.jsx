import React from 'react'
import { Outlet, Link } from "react-router-dom";
import { useState } from 'react';
import sectionInfoData from '../data/sectionInfoData';
import './root.css'

export default function Root() {
    const [activeSiteNavIndex, setActiveSiteNavIndex] = useState(0);
    const [sectionInfo, setSectionInfo] = useState(sectionInfoData)

  return (
    <div className='site-wrapper'>
        <nav className='site-nav'>
            <ul className='site-nav-container'>
                {sectionInfo.map((section, i) => {
                    return (
                        <li 
                            key={i} 
                            onClick={() => setActiveSiteNavIndex(i)} 
                            className='nav-item'
                            style={
                                activeSiteNavIndex === i
                                ? { backgroundColor: "purple", color: "white", padding: '10px 20px', borderRadius: '15px' }
                                : { backgroundColor: "white", color: "black", padding: '10px 20px', borderRadius: '15px' }
                            }
                        >
                            <Link to={section.url}>{section.sectionTitle}</Link>
                        </li>
                    )
                })}
            </ul>
        </nav>

        <main id="main-content">
            <Outlet context={{sectionInfo, setSectionInfo, activeSiteNavIndex}}/>
        </main>
    </div>
  )
}
