import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import "./ModalContainer.css";
import ModalNav from "../Nav/modal/ModalNav";
import ContentPlaceholder from "../Content/ContentPlaceholder";
// Boostrap components
import Modal from "react-bootstrap/Modal";
// import Button from "react-bootstrap/Button"; // used for footer

import ContentWrapper from "../Content/ContentWrapper";

export default function ModalContainer({
  show,
  onHide,
  sectionInfo,
  setSectionInfo,
  activeSiteNavIndex,
}) {
  /*
    Props:
      onHide: () => void;
      show: boolean;
      
      Props supplied from Nexus/Case:
        sectionName: string;
          subSectionName: string;
        currentSection: string;

      state needs:
          activeNavIndex - passed down from site nav
          sectionSubTopics - array of sub topics names pull from content
     
     flow:
        content is created
        sub topic headers are pulled from content and update sitemap state object:


        modal preloads content
        
  */

  const [activeModalNavIndex, setActiveModalNavIndex] =
    useState(activeSiteNavIndex);
  const [APIData, setAPIData] = useState([]);
  // state variable to hold the filtered results based on user search input
  const [filteredResults, setFilteredResults] = useState([]);
  const [searchInput, setSearchInput] = useState("");

  // example API call to illustrate data pull before rendering the section information
  // axios call will be replaced with middleware axios instance available in NEXUS helper functions
  useEffect(() => {
    axios.get(`https://jsonplaceholder.typicode.com/users`).then((response) => {
      setAPIData(response.data);
    });
  }, []);

  const handleSearch = (searchValue) => {
    setSearchInput(searchValue);
    if (searchInput !== "") {
      const filteredData = APIData.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(APIData);
    }
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName="modal-dialog"
      // scrollable="true"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Help</Modal.Title>

        <input
          type="search"
          placeholder="Search..."
          onChange={(e) => handleSearch(e.target.value)}
          className="searchInput"
        />

        {/* TODO: populate this contact info based on the currently logged-in user */}
        <p className="contact-info">Questions? Contact: Test Manager</p>
      </Modal.Header>

      <Modal.Body className="show-grid content-container">
        <div className="nav-container">
          <h1 className="section-header">NEXUS</h1>
          <ModalNav
            menuOpen={true}
            activeModalNavIndex={activeModalNavIndex}
            setActiveModalNavIndex={setActiveModalNavIndex}
          />

          <h1 className="section-header">GENERAL</h1>
          <ModalNav />

          <h1 className="section-header">HARDWARE</h1>
          <ModalNav />
        </div>

        {/* <ContentWrapper>
        {sectionInfo[activeSiteNavIndex].contentElement}
          {console.log(sectionInfo[activeSiteNavIndex].contentElement)}
        </ContentWrapper> */}

        <div className="content-wrapper">
          {sectionInfo[activeModalNavIndex].contentElement}
        </div>
      </Modal.Body>

      {/* <Modal.Footer>
        <Button onClick={onHide}>Close</Button>
      </Modal.Footer> */}
    </Modal>
  );
}

// {/* render logic for filtered search results */}
//           {/* {searchInput.length > 1 ? (
//                     filteredResults.map((result) => {
//                         return (
//                             <div>
//                                     <div>{result.name}</div>
//                                     <div>
//                                         {result.email}
//                                     </div>
//                             </div>
//                         )
//                     })
//                 ) : (
//                     APIData.map((result) => {
//                       return (
//                           <div>
//                                   <div>{result.name}</div>
//                                   <div>
//                                       {result.email}
//                                   </div>
//                           </div>
//                       )
//                   })
//             )} */}
