import React, { useLayoutEffect, useRef } from "react";
import "../Modal/ModalContainer.css";
// Callout Elements
import HighlightCalloutText from "./text-callouts/HighlightCalloutText";
import WarningCalloutText from "./text-callouts/WarningCalloutText";
// helpers
import findElementsByTag from "../../helpers/findElementsByTag";

export default function ContentWrapper({
  children,
  sectionInfo,
  setSectionInfo,
}) {
  const textSectionRef = useRef(null);

  // component grabs requested tags using helper allTagsHTML and sets state w/ data
  useLayoutEffect(() => {
    const filteredElements = findElementsByTag(textSectionRef, "h2");
    console.log(filteredElements);
    // const newSectionInfo = sectionInfo.map(section => {
    //   if (section.sectionTitle ===)
    // })
    // setSectionInfo([...sectionInfo]);
  }, []);

  return (
    <section className="text-container" ref={textSectionRef}>
      {children}
    </section>
  );
}
