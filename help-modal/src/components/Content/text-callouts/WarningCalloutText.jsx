import React from 'react'
import { RiErrorWarningLine } from "react-icons/ri";
import '../../Modal/ModalContainer.css'

export default function WarningCalloutText({children}) {
  return (
    <div className="text_callout-warning">
        <RiErrorWarningLine />
        {` `}
        {children}
    </div>
  )
}
