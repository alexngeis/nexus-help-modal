import React from 'react'
import { MdOutlineNotificationImportant } from "react-icons/md";
import '../../Modal/ModalContainer.css'

export default function HighlightCalloutText({children}) {
    return (
        <div className="text_callout-highlight">
            <MdOutlineNotificationImportant />
            {` `}
            {children}
        </div>
      )
}
