import React, { useContext, useLayoutEffect, useRef } from "react";
import "../../Modal/ModalContainer.css";
// Callout Elements
import HighlightCalloutText from "../text-callouts/HighlightCalloutText";
import WarningCalloutText from "../text-callouts/WarningCalloutText";
// helpers
import findElementsByTag from "../../../helpers/findElementsByTag";
// context
import { SectionContext } from "../../../context/SectionContext";

export default function HomeContent() {
  const textSectionRef = useRef(null);

  const { sectionInfo, setSectionInfo } = useContext(SectionContext);

  // component grabs requested tags using helper findElementsByTag and sets state w/ data
  // useLayoutEffect(() => {
  //   const filteredElements = findElementsByTag(textSectionRef, "h3");
  //   const newSectionInfo = sectionInfo.map((section) => {
  //     if (section.sectionTitle === "Home") {
  //       return { ...section, children: filteredElements };
  //     } else return section;
  //   });

  //   setSectionInfo(newSectionInfo);
  // }, []);

  return (
    <section className="text-container" ref={textSectionRef}>
      <h2>HOME CONTENT</h2>

      <h3 id="home-anchor-1">Sub Topic Header 1</h3>
      <p>
        Brooklyn pinterest messenger bag forage organic heirloom direct trade
        fam four loko typewriter chambray taiyaki 90's slow-carb migas. Mlkshk
        praxis tacos, four dollar toast pour-over pitchfork vice yuccie locavore
        prism vibecession palo santo health goth twee sustainable. Hashtag
        polaroid mixtape tofu chicharrones hoodie, poke praxis cardigan venmo.
        Kickstarter hexagon whatever wayfarers man bun franzen snackwave DSA
        mustache occupy affogato cliche marxism unicorn af. Poutine 3 wolf moon
        godard copper mug lyft swag direct trade irony schlitz locavore butcher
        hexagon etsy. Chartreuse wayfarers subway tile sartorial waistcoat chia
        typewriter mukbang la croix tbh intelligentsia pour-over selfies
        church-key four loko. Kinfolk biodiesel aesthetic thundercats shabby
        chic trust fund. Poke pour-over tumblr twee paleo af umami activated
        charcoal master cleanse scenester stumptown occupy. Pickled distillery
        marfa chillwave pok pok air plant flexitarian edison bulb shoreditch
        street art intelligentsia woke. Aesthetic tbh church-key swag. Air plant
        synth helvetica, tonx raclette photo booth freegan palo santo kogi
        flannel bushwick hoodie PBR&B hot chicken. Prism chillwave hoodie hella
        normcore cold-pressed bruh, cardigan la croix aesthetic single-origin
        coffee. Helvetica next level blue bottle ascot +1, waistcoat jean shorts
        readymade chambray dreamcatcher. Glossier gluten-free plaid, salvia
        taxidermy fit tattooed VHS retro prism portland etsy. Mumblecore
        snackwave aesthetic DIY mixtape celiac meh bushwick kogi yr coloring
        book. Chillwave pickled portland PBR&B wolf craft beer vinyl meditation
        blackbird spyplane pabst. Edison bulb craft beer williamsburg occupy
        heirloom synth tacos single-origin coffee knausgaard street art praxis
        shoreditch master cleanse. Four loko taxidermy narwhal health goth
        occupy Brooklyn retro normcore seitan. Umami art party microdosing
        bespoke DSA, semiotics flannel jawn prism solarpunk truffaut copper mug
        woke. 8-bit lumbersexual umami big mood migas. Subway tile readymade
        tumeric, cray ugh farm-to-table raw denim master cleanse succulents
        helvetica shaman pork belly DIY copper mug. Craft beer tbh gluten-free
        glossier blackbird spyplane portland lumbersexual echo park distillery.
        Succulents pug umami semiotics gluten-free live-edge brunch activated
        charcoal. Narwhal fixie tofu gochujang. Neutra mlkshk cardigan, JOMO
        synth hammock af PBR&B vibecession pickled vaporware crucifix.
      </p>

      <HighlightCalloutText>important highlight text</HighlightCalloutText>

      <h3 id="home-anchor-2">Sub Topic Header 2</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <WarningCalloutText>important warning text</WarningCalloutText>

      <h3 id="home-anchor-3">Sub Topic Header 3</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <WarningCalloutText>important warning text</WarningCalloutText>

      <h3 id="home-anchor-4">Sub Topic Header 4</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3 id="home-anchor-5">Sub Topic Header 5</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3 id="home-anchor-6">Sub Topic Header 6</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3 id="home-anchor-7">Sub Topic Header 7</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3 id="home-anchor-8">Sub Topic Header 8</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>
    </section>
  );
}
