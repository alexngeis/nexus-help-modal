import React, { useLayoutEffect, useRef } from "react";
import "../../Modal/ModalContainer.css";
// Callout Elements
import HighlightCalloutText from "../text-callouts/HighlightCalloutText";
import WarningCalloutText from "../text-callouts/WarningCalloutText";
// helpers
import findElementsByTag from "../../../helpers/findElementsByTag";

export default function AddTestContent({ sectionInfo, setSectionInfo }) {
  const textSectionRef = useRef(null);

  // component grabs requested tags using helper findElementsByTag and sets state w/ data
  // useLayoutEffect(() => {
  //   const filteredElements = findElementsByTag(textSectionRef, "h3");
  //   const newSectionInfo = sectionInfo.map((section) => {
  //     if (section.sectionTitle === "Create New Test") {
  //       return { ...section, children: filteredElements };
  //     } else return section;
  //   });

  //   setSectionInfo(newSectionInfo);
  // }, []);

  return (
    <section className="text-container" ref={textSectionRef}>
      <h2>ADD TEST CONTENT</h2>

      <h3>Sub Topic Header 1</h3>
      <p>
        Bacon ipsum dolor amet beef rump bresaola chuck hamburger, pastrami
        fatback bacon kevin kielbasa venison meatball tail. Venison jerky
        leberkas ham, tail shank porchetta ball tip flank boudin. Kielbasa
        tri-tip ground round, beef tongue turkey cupim biltong bresaola
        porchetta short loin salami alcatra. Buffalo shankle landjaeger, jerky
        andouille pork loin salami.
      </p>

      <HighlightCalloutText>important highlight text</HighlightCalloutText>

      <h3>Sub Topic Header 2</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <WarningCalloutText>important warning text</WarningCalloutText>

      <h3>Sub Topic Header 3</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <WarningCalloutText>important warning text</WarningCalloutText>

      <h3>Sub Topic Header 4</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3>Sub Topic Header 5</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3>Sub Topic Header 6</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3 id="anchor-test">Sub Topic Header 7</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <h3>Sub Topic Header 8</h3>
      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>

      <p>
        Bacon ipsum dolor amet capicola tongue salami buffalo prosciutto.
        Tenderloin pork ham hock, capicola flank tongue alcatra bresaola corned
        beef burgdoggen tri-tip. Filet mignon pancetta beef, cupim ham hock
        tongue chislic tail pork belly jerky cow ball tip sausage. Pancetta
        porchetta sausage tongue drumstick, landjaeger filet mignon doner
        shankle tail pastrami pork belly strip steak. Alcatra turkey short ribs,
        pork chop pork belly t-bone andouille chuck bacon. Salami flank filet
        mignon fatback cupim kielbasa chicken chislic swine tenderloin short
        ribs beef porchetta. Turducken kevin jowl, bresaola filet mignon
        frankfurter rump beef tail shoulder pastrami picanha porchetta. Bacon
        strip steak pork belly fatback tri-tip. Corned beef picanha pastrami
        chuck. Flank beef ribs leberkas capicola doner strip steak. Spare ribs
        drumstick tongue, pork loin pastrami pork belly meatloaf landjaeger
        bacon biltong.
      </p>
    </section>
  );
}
