import React, { useEffect, useState } from "react";
import Accordion from "react-bootstrap/Accordion";
import { SectionContext } from "../../../context/SectionContext";
import { useContext } from "react";
import anchorLinkFromString from "../../../helpers/anchorLinkFromString";

export default function ModalNav({
  menuOpen,
  activeModalNavIndex,
  setActiveModalNavIndex,
}) {
  const { sectionInfo, setSectionInfo } = useContext(SectionContext);

  const topicAmount = 5;
  const topicSections = [];

  // TODO: anchor links, dynamic sub topics based on page headers, and onclick of subtopic should update modal nav index to show new content

  const SubTopicDynamic = () => {
    return (
      <ul>
        {sectionInfo.map((section, sectionIndex) => {
          const children = section.children;
          // console.log("sectionIndex", sectionIndex);
          // console.log(children);
          const childList = children.map((child, i) => {
            // console.log("child", child);
            // console.log(typeof child.textContent);
            // let childText;
            // if (child.textContent) childText = child.textContent;
            // else
            //   childText =
            //     child.subSectionTitle |
            //     `Sub Topic ${i} ${section.sectionTitle}`;
            // const anchorLink = anchorLinkFromString(childText);
            // console.log(anchorLink);
            return (
              <li key={i}>
                {/* <a href={"test"} onClick={() => setActiveModalNavIndex(i)}>
                  {child.textContent}
                </a> */}
                <p
                  onClick={() => {
                    console.log(sectionIndex);
                    setActiveModalNavIndex(sectionIndex);
                  }}
                >
                  {child.textContent}
                </p>
              </li>
            );
          });

          return childList;
        })}
      </ul>
    );
  };

  const SubTopic = () => {
    return (
      <ul>
        <li>Subtopic 1</li>
        <li>Subtopic 2</li>
        <li>Subtopic 3</li>
        <li>Subtopic 4</li>
      </ul>
    );
  };

  const Topics = (topicNumber) => {
    return (
      <Accordion.Item eventKey={topicNumber} key={topicNumber}>
        <Accordion.Header>{`Topic ${topicNumber + 1}`}</Accordion.Header>
        <Accordion.Body>
          <SubTopic />
        </Accordion.Body>
      </Accordion.Item>
    );
  };

  for (let index = 0; index < topicAmount; index++) {
    topicSections.push(Topics(index));
  }

  return (
    <nav style={{ marginBottom: "20px" }}>
      {menuOpen ? (
        <Accordion defaultActiveKey={activeModalNavIndex} flush>
          {sectionInfo.map((section, i) => {
            return (
              <Accordion.Item eventKey={i} key={i}>
                <Accordion.Header>
                  {sectionInfo[i].sectionTitle}
                </Accordion.Header>
                <Accordion.Body>
                  {/* <SubTopicDynamic /> */}
                  {/* <SubTopicChildren /> */}
                  <ul>
                    {section.children.map((child, childIndex) => {
                      return (
                        <li
                          key={childIndex}
                          onClick={() => setActiveModalNavIndex(i)}
                        >
                          <a href={`#${child.anchorLink}`}>
                            {child.subSectionTitle}
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </Accordion.Body>
              </Accordion.Item>
            );
          })}
        </Accordion>
      ) : (
        <Accordion flush>
          {topicSections.map((section) => {
            return section;
          })}
        </Accordion>
      )}
    </nav>
  );
}
