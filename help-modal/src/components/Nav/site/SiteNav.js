import React, { useState } from "react";

export default function SiteNav({ navItems, activeIndex, setActiveIndex }) {
  return (
    <>
      {navItems.map((navItem, i) => {
        return (
          <button
            key={i}
            onClick={() => setActiveIndex(i)}
            style={
              activeIndex === i
                ? { backgroundColor: "purple", color: "white" }
                : { backgroundColor: "white", color: "black" }
            }
          >
            {navItem.title}
          </button>
        );
      })}
    </>
  );
}
