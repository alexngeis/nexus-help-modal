import HomeContent from "../components/Content/Nexus/HomeContent";
import AddHardwareContent from "../components/Content/Nexus/AddHardwareContent";
import AddTestContent from "../components/Content/Nexus/AddTestContent";
import ActiveTestsContent from "../components/Content/Nexus/ActiveTestsContent";
import CompletedTestsContent from "../components/Content/Nexus/CompletedTestsContent";

const sectionInfoData = [
  {
    sectionTitle: "Home",
    url: "/home",
    contentElement: <HomeContent />,
    children: [
      {
        subSectionTitle: "Sub Topic Header 1",
        anchorLink: "home-anchor-1",
      },
      {
        subSectionTitle: "Sub Topic Header 2",
        anchorLink: "home-anchor-2",
      },
      {
        subSectionTitle: "Sub Topic Header 3",
        anchorLink: "home-anchor-3",
      },
      {
        subSectionTitle: "Sub Topic Header 4",
        anchorLink: "home-anchor-4",
      },
      {
        subSectionTitle: "Sub Topic Header 5",
        anchorLink: "home-anchor-5",
      },
      {
        subSectionTitle: "Sub Topic Header 6",
        anchorLink: "home-anchor-6",
      },
      {
        subSectionTitle: "Sub Topic Header 7",
        anchorLink: "home-anchor-7",
      },
      {
        subSectionTitle: "Sub Topic Header 8",
        anchorLink: "home-anchor-8",
      },
    ],
  },
  {
    sectionTitle: "Add Hardware",
    url: "/hardware/create",
    contentElement: <AddHardwareContent />,
    children: [
      {
        subSectionTitle: "Sub Topic Header 1",
        anchorLink: "add-hardware-anchor-1",
      },
      {
        subSectionTitle: "Sub Topic Header 2",
        anchorLink: "add-hardware-anchor-2",
      },
      {
        subSectionTitle: "Sub Topic Header 3",
        anchorLink: "add-hardware-anchor-3",
      },
      {
        subSectionTitle: "Sub Topic Header 4",
        anchorLink: "add-hardware-anchor-4",
      },
      {
        subSectionTitle: "Sub Topic Header 5",
        anchorLink: "add-hardware-anchor-5",
      },
      {
        subSectionTitle: "Sub Topic Header 6",
        anchorLink: "add-hardware-anchor-6",
      },
      {
        subSectionTitle: "Sub Topic Header 7",
        anchorLink: "add-hardware-anchor-7",
      },
      {
        subSectionTitle: "Sub Topic Header 8",
        anchorLink: "add-hardware-anchor-8",
      },
    ],
  },
  {
    sectionTitle: "Create New Test",
    url: "/test/create",
    contentElement: <AddTestContent />,
    children: [],
  },
  {
    sectionTitle: "Active Tests",
    url: "/test/active",
    contentElement: <ActiveTestsContent />,
    children: [],
  },
  {
    sectionTitle: "Completed Tests",
    url: "/test/complete",
    contentElement: <CompletedTestsContent />,
    children: [],
  },
];

export default sectionInfoData;
// type SiteMap = {

// }

// home
// add hardware
// create new test
// active tests
// completed tests
